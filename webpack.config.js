let path = require('path');
let webpackMerge = require('webpack-merge');

//root directory
let rootDir = path.resolve(__dirname);

//import webpack config from ionic
let ionicWebPackConfig = require(path.resolve(rootDir, "node_modules", "@ionic", "app-scripts", "config", "webpack.config"));
console.log(rootDir);
var phaserModule = path.join(rootDir, "node_modules", "phaser-ce");
var phaser = path.join(phaserModule, "build", "custom", "phaser-split.js");
var pixi = path.join(phaserModule, "build", "custom", "pixi.js");
var p2 = path.join(phaserModule, "build", "custom", "p2.js");

let webpackExtension = {
    resolve: {
        alias: {
            "pixi": pixi,
            "p2": p2,
            "phaser-ce": phaser
        }
    },
    module: {
        loaders: [{
            test: /pixi\.js/,
            loader: "expose-loader?PIXI"
        }, {
            test: /pahser-split\.js/,
            loader: "expose-loader?Phaser"
        }, {
            test: /p2\.js/,
            loader: "expose-loader?p2"
        }]
    }
};

module.exports = {
    dev: webpackMerge.smart(ionicWebPackConfig.dev, webpackExtension),
    prod: webpackMerge.smart(ionicWebPackConfig.prod, webpackExtension)
};