import "pixi";
import "p2";
import * as Phaser from "phaser-ce";


/**
 * @export
 * @class Game
 * @extends {Phaser.Game}*/
export class Game extends Phaser.Game {
    /**
     * @member Game
     * */
    constructor() {
        super(800, 600, Phaser.CANVAS, "game", null);
    }
}