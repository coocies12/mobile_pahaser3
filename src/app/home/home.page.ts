import {Component} from '@angular/core';
import {NavController} from "@ionic/angular";

import {Game} from "../game/game";

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {

    /**
     * @private
     * @type {Game}
     * @memberOf HomePage
     * */
    private gameInstance: Game;


    /**
     * @param {NavController} navController
     * @memberOf HomePage
     * */
    constructor(public navController: NavController) {
        this.gameInstance = new Game();
    }

}
